/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.reflectionmanager;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.hiik.edu.annotations.ExcelField;
import ru.hiik.entitycore.entity.professor.Professor;
import ru.hiik.entitycore.entity.student.Student;

/**
 *
 * @author vaganovdv
 */
public class ReflectionManagerTest
{
    // Создание экземпляра тестируемого класса 
    private ReflectionManager manager = new ReflectionManager();
    
    // Проверка существования экземпляра 
    @Test
    public void createInstance()
    {
        // Выяснить, что экземпляр не нулевой            сообщение об ошибке 
        //          |                                       |
        Assert.assertNotNull(manager, "Ошибка создания экземпляра класса  {ReflectionManager}");
    }
    
    
    @Test(dependsOnMethods ={"createInstance"})
    public void getListOfFiledsNamesForStudent()
    {
        Optional <List<String>> fieldNamesOpt = Optional.empty();
   
        fieldNamesOpt = manager.getFieldNames(Student.class);
        Assert.assertTrue(fieldNamesOpt.isPresent(), "Не удалось получить список имен полей класса {Student}");
        Assert.assertEquals(fieldNamesOpt.get().size(), 7, "Неверное количество имен полей класса {Student}");
        System.out.println("");
        System.out.println("Имена полей класса Student");
        fieldNamesOpt.get().stream().forEach(name ->
        {
            System.out.println("Поле :"+name);
        });

    }
    
    @Test(dependsOnMethods ={"getListOfFiledsNamesForStudent"})
    public void getListOfFiledsNamesForProfessor()
    {
        // Формирование опционального списка объектов типа String List<String>
        Optional <List<String>> fieldNamesOpt = Optional.empty();
   
        fieldNamesOpt = manager.getFieldNames(Professor.class);
        
        
        Assert.assertTrue(fieldNamesOpt.isPresent(), "Не удалось получить список полей класса {Student}");
        
        // Вызов функции get возвращает List<String>
        // Выяснить, что в списке List<String> содержится 2 элемента
        Assert.assertEquals(fieldNamesOpt.get().size(), 2, "Неверное количество полей класса {Student}");
        
        System.out.println("");
        System.out.println("Имена полей Professor");
        fieldNamesOpt.get().stream().forEach(name ->
        {
            System.out.println("Поле :"+name);
        });
    }
    
    
    @Test(dependsOnMethods ={"getListOfFiledsNamesForStudent"})
    public void getListOfFiledsForStudent()
    {
        Optional <List<Field>> fieldOpt = Optional.empty();
        fieldOpt = manager.getFieldList(Student.class);
        Assert.assertTrue(fieldOpt.isPresent(), "Не удалось получить список полей класса {Student}");
        System.out.println("");
        System.out.println("Список полей класса Student");
        fieldOpt.get().stream().forEach(field ->
        {
            String name = field.getName();
            String type = field.getType().getSimpleName();
            Annotation[] annotation = field.getAnnotations();
            List<Annotation> annotationList = new ArrayList<>();
            for (Annotation ann : annotation)
            {
                annotationList.add(ann);
            }
            String out = String.format("%-20s %-15s %-45s", "["+name+"]", type, "@ = "+annotationList);
            System.out.println(out);
            
            
            annotationList.stream().forEach( a -> {
            
                if ( a instanceof ExcelField )
                {
                    ExcelField  excelField = (ExcelField) a;
                    System.out.println("Имя аннотации: "+excelField.name());
                    System.out.println("Индекс:        "+excelField.index());
                }   
            
            });
            
        });
         
    }
    
}
