/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.hiik.reflectionmanager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.reflect.FieldUtils;

/**
 *
 * @author vaganovdv
 */
public class  ReflectionManager 
{
    
    /**
     * Получение списка полей для класса, указанного в качестве параметра
     * @param clazz класс для обработки 
     * @return  Optional 
     */
    public Optional<List<String>> getFieldNames(Class<?> clazz)
    {
        Optional<List<String>> fieldNamesOpt = Optional.empty();
        List<String> filedNames = new ArrayList<>();

        // Получение у класса clazz списка полей 
        // Вызов FieldUtils.getAllFieldsList возвращает список полей 
        // в виде списка элестпляров класса Field
        // Обрабатывается список как поток
        FieldUtils.getAllFieldsList(clazz).stream().forEach(f ->
        {
            filedNames.add(f.getName());
        });

        if (!filedNames.isEmpty())
        {
            fieldNamesOpt = Optional.of(filedNames);
        }
        return fieldNamesOpt;
    }
      

    /**
     * Функция, возвращающая имя, тип и аннотацию поля в массиве 
     */
    

    
    /**
     * Получение списка полей
     * @param clazz
     * @return 
     */
    public  Optional<List<Field>> getFieldList(Class<?> clazz){
        List<Field> fieldList = new ArrayList<>();
        Optional<List<Field>> fieldListOpt = Optional.empty();
        FieldUtils.getAllFieldsList(clazz).stream().forEach(field ->{
            fieldList.add(field);
        });
           if (!fieldList.isEmpty()){
            fieldListOpt = Optional.of(fieldList);
        }
        return fieldListOpt;
    } 
}
